use std::fs;

pub fn load_file(filename: &str) -> String {
    fs::read_to_string(filename).expect("Something went wrong reading the file")
}

pub fn split(file_content: &str) -> Vec<&str> {
    let chars_vect: Vec<&str> = file_content.split_whitespace().collect();
    chars_vect
}

pub fn split_on_CR(file_content: &str) -> Vec<&str> {
    let chars_vect: Vec<&str> = file_content.split('\n').collect();
    chars_vect
}