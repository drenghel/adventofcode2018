pub fn is_different_by_one_char(input: &str, compared_with: &str) -> bool {
    assert_eq!(
        input.len(),
        compared_with.len(),
        "is_different_by_one_char inputs most be of same len !"
    );

    let mut number_of_same_char = 0;
    for (index, a_char) in input.chars().enumerate() {
        if compared_with.chars().nth(index).unwrap() == a_char {
            number_of_same_char += 1;
        }
    }

    input.len() - 1 == number_of_same_char
}
pub fn get_common_string(input: &str, compared_with: &str) -> String {
    let mut res = String::from(input);
    for (index, a_char) in input.chars().enumerate() {
        if a_char != compared_with.chars().nth(index).unwrap() {
            res.remove(index);
        }
    }
    res
}
