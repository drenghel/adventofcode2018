extern crate utilities_lib;

use std::collections::HashMap;
use utilities_lib::load_file;

mod part_one;
mod part_two;

const FILENAME: &str = "res/input";

fn main() {
    let content = load_file(FILENAME);
    let list_of_slices = utilities_lib::split(&content);

    let mut stats_hash = HashMap::new();
    part_one::compile_stats(&mut stats_hash, &list_of_slices);
    println!(
        "Hash result is : {} ",
        part_one::compute_mini_hash(&stats_hash)
    );

    for compared in list_of_slices.iter() {
        for elem in list_of_slices.iter() {
            if part_two::is_different_by_one_char(elem, compared) {
                println!("Found one ! it's {} and {}", compared, elem);
                println!(
                    "The common part being {} ",
                    part_two::get_common_string(compared, elem)
                );
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn loading() {
        let content = load_file(FILENAME);
        let list_of_inputs = part_one::split(&content);

        let nb = list_of_inputs.len();
        assert_eq!(nb, 250);
    }

    #[test]
    fn check_letters() {
        let input = "qqqqsd ";
        let count = part_one::count_letter(input);

        let res = count.get("d");
        if res.is_some() {
            assert_eq!(*res.unwrap(), 1);
        } else {
            panic!("That key isn't in there !");
        }

        let res = count.get("q");
        if res.is_some() {
            assert_eq!(*res.unwrap(), 4);
        } else {
            panic!("That key isn't in there !");
        }

        let res = count.get("s");
        if res.is_some() {
            assert_eq!(*res.unwrap(), 1);
        } else {
            panic!("That key isn't in there !");
        }
    }

    #[test]
    fn test_id_type_recoqnition_three() {
        let mut input = HashMap::new();
        input.insert("w", 3);
        input.insert("d", 1);
        input.insert("s", 0);

        let id_type = part_one::read_id_type(&input);

        match id_type {
            part_one::IdType::OnlyThreeIdentical => assert!(true),
            _ => panic!("Failed !"),
        }
    }

    #[test]
    fn test_id_type_recoqnition_two() {
        let mut input = HashMap::new();
        input.insert("w", 0);
        input.insert("d", 1);
        input.insert("s", 2);

        let id_type = part_one::read_id_type(&input);

        match id_type {
            part_one::IdType::OnlyTwoIdentical => assert!(true),
            _ => panic!("Failed !"),
        }
    }

    #[test]
    fn test_id_type_recoqnition_irrelevant() {
        let mut input = HashMap::new();
        input.insert("w", 0);
        input.insert("d", 1);
        input.insert("s", 0);

        let id_type = part_one::read_id_type(&input);

        match id_type {
            part_one::IdType::Irrelevant => assert!(true),
            _ => panic!("Failed !"),
        }
    }

    #[test]
    fn test_id_type_recoqnition_both() {
        let mut input = HashMap::new();
        input.insert("w", 0);
        input.insert("d", 3);
        input.insert("s", 2);

        let id_type = part_one::read_id_type(&input);

        match id_type {
            part_one::IdType::Both => assert!(true),
            _ => panic!("Failed !"),
        }
    }

    #[test]
    fn test_test_similar_str_by_one_char() {
        let input = "foo";
        let compared = "foi";

        let res = part_two::is_different_by_one_char(input, compared);

        assert!(res);
    }

    #[test]
    fn test_get_commun_string() {
        let input = "pwet";
        let compared = "pwets";

        let res = part_two::get_common_string(input, compared);

        assert_eq!(res, String::from("pwet"));
    }
}
